package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        Iterator itX = x.iterator();
        Iterator itY = y.iterator();

        Object objX;
        int foundElements = 0;

        while (itX.hasNext()){
            objX = itX.next();
            if (!itY.hasNext()) {
                return false;
            }
            while (itY.hasNext()) {
                if (objX.equals(itY.next())) {
                    foundElements++;
                    break;
                }
            }
        }

        return foundElements == x.size();
    }
}
