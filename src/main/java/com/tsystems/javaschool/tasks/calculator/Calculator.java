package com.tsystems.javaschool.tasks.calculator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class Calculator {

    private final HashMap<Character, Integer> operatorSigns = new HashMap<>();
    {
        operatorSigns.put('(', 0);
        operatorSigns.put(')', 1);
        operatorSigns.put('+', 2);
        operatorSigns.put('-', 2);
        operatorSigns.put('*', 3);
        operatorSigns.put('/', 3);
    }

    private final Set<Character> numberCharacters = new HashSet<>();
    {
        numberCharacters.add('0');
        numberCharacters.add('1');
        numberCharacters.add('2');
        numberCharacters.add('3');
        numberCharacters.add('4');
        numberCharacters.add('5');
        numberCharacters.add('6');
        numberCharacters.add('7');
        numberCharacters.add('8');
        numberCharacters.add('9');
        numberCharacters.add('.');
    }
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.length() == 0) {
            return null;
        }

        Stack<Character> operators = new Stack<>();
        Stack<Double> result = new Stack<>();

        int i = 0;
        while(i < statement.length()) {
            if(operatorSigns.containsKey(statement.charAt(i))) {
                if (statement.charAt(i) != '(') {
                    while (!operators.empty()
                            && operatorSigns.get(statement.charAt(i)) <= operatorSigns.get(operators.peek())) {
                        if (!addOperatorToResultStack(result, operators.pop())){
                            return null;
                        }
                    }
                }
                if (statement.charAt(i) == ')' && (operators.empty() || operators.pop() != '(')) {
                    return null;
                }
                if (statement.charAt(i) != ')') {
                    operators.push(statement.charAt(i));
                }
                i++;
            } else {
                StringBuilder nextNumberBuilder = new StringBuilder();
                boolean wasDot = false;
                while (i < statement.length() && !operatorSigns.containsKey(statement.charAt(i))) {
                    if (!numberCharacters.contains(statement.charAt(i)) || statement.charAt(i) == '.' && wasDot) {
                        return null;
                    }
                    if (statement.charAt(i) == '.') {
                        wasDot = true;
                    }
                    nextNumberBuilder.append(statement.charAt(i));
                    i++;
                }
                if (nextNumberBuilder.charAt(0) == '.' || nextNumberBuilder.charAt(nextNumberBuilder.length() - 1) == '.') {
                    return null;
                }
                result.push(Double.parseDouble(nextNumberBuilder.toString()));
            }
        }

        while (!operators.empty()) {
            if(!addOperatorToResultStack(result, operators.pop())){
                return null;
            }
        }

        double returnValue = result.pop();
        if (Math.abs(returnValue) - (long) Math.abs(returnValue) == 0) {
            return String.valueOf((long) returnValue);
        } else {
            return String.valueOf((double) Math.round(returnValue * 10000d) / 10000d);
        }
    }

    private double performOperation(double first, double second, char operator) {
        if (operator == '+') {
            return first + second;
        } else if (operator == '-') {
            return first - second;
        } else if (operator == '*') {
            return first * second;
        } else {
            return first / second;
        }
    }

    private boolean addOperatorToResultStack(Stack<Double> stack, char operator) {
        if (stack.size() < 2) {
            return false;
        }
        double second = stack.pop(), first = stack.pop();
        if (operator == '/' && second == 0.0) {
            return false;
        }
        stack.push(performOperation(first, second, operator));
        return true;
    }
}
