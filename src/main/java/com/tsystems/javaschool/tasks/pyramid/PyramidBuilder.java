package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.PriorityQueue;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int currentRequiredAmount = 3;
        int nextLayerSurplus = 3;
        PriorityQueue<Integer> elementsSorted;
        try {
            elementsSorted = new PriorityQueue<>(inputNumbers.size());
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }

        for (Integer number : inputNumbers) {
            if (number == null) {
                throw new CannotBuildPyramidException();
            }
            elementsSorted.add(number);
            if (elementsSorted.size() > currentRequiredAmount) {
                currentRequiredAmount += nextLayerSurplus++;
            }
        }

        if (elementsSorted.size() < currentRequiredAmount) {
            throw  new CannotBuildPyramidException();
        }

        int[][] pyramid;

        try {
             pyramid = new int[nextLayerSurplus - 1][2 * nextLayerSurplus - 3];
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
        int indent = nextLayerSurplus - 2;
        for (int i = 0; i < pyramid.length; i++) {
            int j;
            for (j = 0; j < indent; j++) {
                pyramid[i][j] = 0;
            }
            j = indent;
            while(j < pyramid[i].length - indent) {
                pyramid[i][j] = elementsSorted.poll();
                if (j + 1 < pyramid[i].length) {
                    pyramid[i][j + 1] = 0;
                }
                j += 2;
            }
            for (j = pyramid[i].length - indent; j < pyramid[i].length; j++) {
                pyramid[i][j] = 0;
            }
            indent--;
        }

        return pyramid;
    }
}
